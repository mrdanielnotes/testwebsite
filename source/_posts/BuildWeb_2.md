---
title: 架設網站三部曲(2/3)
tags: [git, gitlab]
categories: [免費架站]
date: 2020-07-28 18:00:00
---

架設站台所使用的git最基礎指令，GitLab操作，手把手教學

<!-- more-->

---



# Git基礎指令

git基礎指令如下

建立基本 **儲存庫(Repository)** (建立git基本檔案)

````python
git init
````

複製 git 儲存庫至本地端 (工作目錄)

```python
git clone "https://gitlab.com/xxx/xxx.git"
```

建立遠端連線

```python
git remote add origin "https://gitlab.com/xxx/xxx.git"
```

查看遠端連線

```
git remote
```

查看目前資料夾有哪些變更

````python
git status
````

將所有變更一次新增至 **暫存區(Staging Area)**

````python
git add .
````

將 **暫存區** 的檔案上傳至 **儲存庫(Repository)**

````python
git commit -m "My first website's commit"
````

將所有變更一次上傳至 **儲存庫(Repository)** ，略過 **暫存區(Staging Area)**

```python
git commit -a
```

上傳至 git，origin自定義遠端名稱

```python
git push origin master
```

---



# 概念說明 (Concept Explain)

兩種方式可以放置儲存庫，藍色和綠色的差異



<img src="../images/BuildWebsite/git1.png" alt="git說明" style="zoom: 80%;" />



工作目錄(Working Directory) = 你所使用的資料夾目錄，通常是指 **小黑窗** 前方的位置 ，">"前面的目錄

![image-20200727115900388](../images/BuildWebsite/git2.png)

暫存區(Staging Area) = 暫時存放的區域，欲改變的項目，快速暫存，不需要提交所有項目

儲存庫(Repository)  = 儲存改變的項目，欲上傳的項目

---



# GitLab - 新增專案 (Add Project)

進入專案畫面

<img src="../images/BuildWebsite/git3.png" alt="image-20200727144337727" style="zoom: 50%;" />

新增

<img src="../images/BuildWebsite/git4.png" alt="image-20200727144504991" style="zoom: 50%;" />



建立

<img src="../images/BuildWebsite/git5.png" alt="image-20200727144930234" style="zoom:50%;" />

---



# GitLab - 打包 (Check out)

![image-20200727145349064](../images/BuildWebsite/git6.png)

隨意建置一個資料夾放置後續所需要的檔案，並開啟

<img src="../images/BuildWebsite/git7.png" alt="image-20200727114724552" style="zoom:67%;" />

點選，目錄的部分，就可以知道當前位置的路徑，ctrl + c 複製路徑

<img src="../images/BuildWebsite/git8.png" alt="image-20200727114657602" style="zoom: 67%;" />

開啟小黑窗

```python
windows + r  輸入 cmd
```

複製與建立 **儲存庫 (Repository)**

```python
git clone "https://gitlab.com/adannotes/mynotes.git"
```

新增至工作目錄下，再進行 git clone

<img src="../images/BuildWebsite/git9.png" alt="image-20200727145959857" style="zoom:67%;" />

Notes : **過程中可能需要打gitlab的帳密** 

完成後，會發現資料夾中沒有任何東西，這是正常的，因為現在專案中也沒有任何東西 

(但其實有隱藏檔案協助管控的)

---



# GitLab - 上傳 (Upload)

新增一個 **README.md** 此檔案預設為專案介紹

接著在 **小黑窗** 中下指令

```python
cd mynotes

echo "Lets start git" > README.md

git status
```

<img src="../images/BuildWebsite/git10.png" alt="image-20200727153735078" style="zoom:67%;" />

接著我們將專案介紹，上傳至GitLab中的專案

首先必須先設定user.email和user.name，只有第一次要設定

```python
git config --global user.email "you@email.com"
git config --global user.name "your name"
```

接著操作上傳儲存庫

```python
git commit -m "project introduction"
```

上傳至Git

```
git push origin master
```

origin為自訂名稱，可使用下方查看目前擁有的遠端名稱

```
git remote 
```



回到GitLab專案中，可以到你剛剛建立的 **README.md** 已經被上傳上來了

![image-20200727155500329](../images/BuildWebsite/git11.png)

操作結束，恭喜你，成功上傳一個專案介紹

---



# 結語 (Conclusion)

從無到有的Git基本操作，沒有太艱深的概念，照著一步一步操作就能夠完成

下一篇使用Hexo，開始搭建網站，讓世界看到你的努力