---
title: 總覽 (Overview)
date: 2020-06-30 09:12:57
tags: [Overview]
categories: [Overview]
---

記錄所有概要與索引，個人備忘錄

<!-- more -->

1. 建置Hexo and GitLab建置

2. 設定Theme : Next 樣式

3. 標籤雲 (TagCloud)

   

4. ReadMore

5. [darkmodel_js](https://github.com/dog-2/hexo-theme-next/commit/c462fff479ef4e65f3c39c27b2f624f0bc37105f)

   **next > config_yml 、layout/_script/verdors.swig**

   修改其css，增加dark_layer 的z-index : 1，能夠覆蓋到文章內容之上

   但在暗黑模式，文章圖片也會變色，導致不精準 > 尚未解決

   

6. (無法根治) 關於Hexo next 主題，文章導覽，繁體中文無法正常使用問題，英文是可以正常導覽的，巨坑

   themes\next\source\js\utils.js  偵錯無法解決

   ```python
   # 修改前
   var target = document.getElementById(event.currentTarget.getAttribute('href').replace('#', 
   ''));
   
   # 修改後，增加 decodeURI 包住 event.currentTarget.getAttribute('href')
   # 括弧記得清楚看到
   var target = document.getElementById(decodeURI(event.currentTarget.getAttribute('href')).replace('#', ''));
   ```

   

7. (後紀) 有可能是因為目前猜測是 darkmodel_js影響，導致導覽列無法順暢，重裝一步一步增加功能偵測錯誤，暫時放置問題。

8. RL，增加障礙物避開訓練

9. Yolo 模型與商品辨識概要與描述

10. NN 類神經介紹

11. 增加瀏覽人數

    1.  **/theme/next/_config.yml** 開啟後

       搜尋 **busuanzi_count**  
    
       將 enable: false 改為 true
    
       ```python
       busuanzi_count:
         enable: true  #修改此處，是否啟用
         total_visitors: true  #總瀏覽人數
         total_visitors_icon: fa fa-user  #總瀏覽人數圖示
         total_views: true  #總觀看人數
         total_views_icon: fa fa-eye  #總觀看人數圖示
         post_views: true  #文章瀏覽次數
         post_views_icon: fa fa-eye  #文章瀏覽次數圖示
       ```
    
       
    
       搜尋 **footer:**，增加下方區塊
    
       ```python
       # visitors count
       counter: true
       ```
    
    2.  **/themes\next\layout\_partials** 中的 **footer.swig** 檔案中，增加
    
       ```javascript
       {% if theme.footer.counter %}
           <script async src="//dn-bstatics.qbox.me/busuanzi/2.3/busuanzi.pure.mini.js"></script>
       {% endif %}
       ```
    
       Note : 若使用Hexo 本地端運行( Localhost ) 無法正常顯示，屬於正常現象，發布至公開網址、網域就會正常顯示。
    
    
    
    
