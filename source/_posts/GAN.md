---
title: Generative Adversarial Networks 生成對抗網路
date: 2020-08-06 09:21:51
tags: [Machine Learning, ML x GAN]
categories: [Artificial intelligence]
---

來到這個社會上就是不斷的在學習，不管是跟前輩學習、跟長輩學習、跟同輩學習、或是 **跟自己學習**？？

說到 **跟自己學習**，就必須要提到一個類神經架構，叫做 **生成對抗網路 (Generative Adversarial Networks, GAN)**

<!-- more -->

## 什麼是 **生成對抗網路 (GAN)**

他就是一個自己跟自己學習的範例

如果想吃速食，請往下到最後的 **故事解釋** 會幫助你更快速的理解



可以分為兩個部分

1. Generator : 生成者，以下代稱 G
2. Discriminator : 鑑別者，以下代稱 D

G這位仁兄，要做的工作是產出一個 **假的圖片**

D這位智者，需要判定 G仁兄 產出的圖片是真的還假的，並做一個標記

以下方圖例做英文解釋

1. Noise = 雜湊，可把它當作一個亂數做思考
2. Generator = G仁兄
3. Fake Image = 假的圖片
4. Real Image = 真的圖片
5. Discriminator = D智者
6. Real / Fake = 標記

<img src="../images/GAN/GAN.png" alt="_GAN" style="zoom:67%;" />

## 深入解釋

根據原文中的演算法可看出幾個概念，以淺白的意思解釋如下 (依照上到下的順序介紹)

1. k = D 的步數
2. z = noise (就是亂數)
3. x = real images (真實的圖片數據，以色階的方式表示每一個像素點作為數據)
4. 有兩個更新公式，分別更新 G (Generator) 和 D (Discriminator)

在演算法上方有一段文字，其中提到 k的解釋，作者表示，k 是 D 數據的步數，當k = 1的時候，是在實驗中最便宜的選擇

**白話文：k =  1，D 數據一個一個跑迴圈訓練** ，是一點都不貴的選擇

<img src="../images/GAN/image-20200806144307243.png" alt="image-20200806144307243" style="zoom:50%;" />

整個架構可分為兩大塊

1. D 訓練與Loss更新
2. G 訓練與Loss更新

Notes：依照一次 **疊代(Iteration)** 來說演算法順序來說，是訓練並更新完 D 之後，才訓練更新 G

**一次疊代 = 跑一次迴圈的俗稱**



### D 訓練與更新

了解了每一個英文變數的意義後

根據D的更新公式

<img src="../images/GAN/image-20200806145937289.png" alt="image-20200806145937289" style="zoom: 50%;" />

這樣就很好理解了，以下是他的更新公式簡化版

```python
D_Loss = D(x) + log(1 - D(G(z)))
```

分別解釋一下

```python
# D(x)有點難懂，把它當成下方的function理解
def D(data_image):
    # 做一些計算和公式 (類神經)
    return D_List
```

```python
# G(z)也是一樣的意思，z = noise (亂數)
def G(noise):
    # 做公式計算 (類神經)
    return G_List
```

如果以C#來說就是

```c#
public List<int> D(List<int> data_image){
    //計算公式
    return D_List;
}
public List<int> G(List<int> noise){
    //計算公式
    return G_List;
}
```

以這樣的意思表示，會更清楚

```python
D_Loss = D(x) + log(1 - D(G(z)))
# D的誤差值 = (D 鑑別 真實圖片 出來的數據) + log(1 - D 鑑別 G 生成的圖片)
# 看到log很害怕，別擔心，你就把它當成 乘法 或 除法就好了
# 就只是一種算法，在解釋上可以直接選擇性忽略
```



### G 訓練與更新

同樣的解釋一下，G 的更新公式

<img src="../images/GAN/image-20200806145954369.png" alt="image-20200806145954369" style="zoom:50%;" />

簡化版

```python
G_Loss = log(1 - D(G(z)))
# G的誤差值 = log(1 - D 鑑別 G 生成的圖片)
# log 可以暫時忽略，會比較容易懂
```



### 白話文撰寫GAN演算法

```
訓練次數迴圈{
    D訓練的迴圈{
        更新D
    }
    更新G
}
```

完整GAN架構圖變成

<img src="../images/GAN/GAN_Full.png" alt="GAN_Full" style="zoom:50%;" />



## 故事解釋

故事有兩個人，G仁兄和D智者

G仁兄打算賣畫為生，D智者跟他說，一幅真的畫100w，一幅假的畫1w

G仁兄便答應了，開始他的作畫旅程

但是G仁兄總是畫不好，因此詢問D智者，該怎麼才能畫得更好

得到了D智者建議，G仁兄一邊參考建議，一邊學習如何畫得更好

期待總有一天，G仁兄能夠畫的跟真的一樣好

<img src="../images/GAN/story.png" alt="story" style="zoom: 50%;" />



## 結語

以人類的角度來說，G 和 D 都是在同一支程式底下，也就是自己跟自己打架，好比永春拳 (????

自己告訴自己哪裡不好，並且學習和進步，我想這應該就是GAN最淺白的解釋了

另外

小弟不才，目前並沒有Source Code可以提供，之後再補上，下台一鞠躬



## Reference

[lan J. Goodfellow et al. (2014) Generative Adversarial Networks](https://arxiv.org/abs/1406.2661)

