---
title: 架設網站三部曲(3/3)
tags: [hexo, git, gitlab]
categories: [免費架站]
date: 2020-07-30 12:00:00
---

學習Hexo指令，並上傳GitLab，看見最後成果

<!-- more -->



# 文章概要

使用Hexo搭建網站，操作指令圖示詳解，以Windows為範例，簡易成形。

1. Hexo 基本指令 (Hexo Basic Instruction)
2. 本機搭建 (工作目錄搭建) 網站 (Building Website)
3. 上傳至GitLab (Upload to GitLab)

---



# 前情提要

架設網站三部曲前兩部曲屬於安裝環境和基本指令的教學

1. 環境 : npm, git, Hexo
2. git 基本指令

還沒熟悉的，可以去 [架設網站三部曲 (1/3)](https://mrdanielnotes.gitlab.io/2020-07-27-BuildWeb_1/) [架設網站三部曲(2/3)](https://mrdanielnotes.gitlab.io/2020-07-28-BuildWeb_2//) 跟著操作一下



# Hexo Basic Instruction

```
hexo init <folder>  //創建一個<folder>並下載範本
hexo s  |  hexo server  //啟動伺服器，兩個指令都可以
hexo new page tags  //新增一個Layout目錄 source\tags\index.md
hexo new tags  //新增一篇文章  路徑 : source\_posts\tags.md

hexo deploy  //可設定遠端站台，直接發布 (此處暫不使用)
```

---



# Hexo Building Website

本機搭建 (工作目錄搭建) 網站

打開 **小黑窗(cmd)** 

```python
windows + r, 輸入cmd
```

接著操作

```python
hexo init Web  # 創建一個資料夾並下載範本
cd Web  # 將工作目錄轉移至Web資料夾下
npm install  # 安裝npm至目錄下
```

結果圖，請笑納

<img src="../images/BuildWebsite/image-20200730145554788.png" alt="image-20200730145554788" style="zoom:80%;" />

既然下載完範本了，就直接啟動看看

```
hexo s
```

在瀏覽器上，輸入預設的路徑

```
localhost:4000
```

這時候就會有一個網站雛形給你 皮乓己咧

可以看一下網頁裡面的內容，算是Hexo的基本指令

<img src="../images/BuildWebsite/image-20200730150129752.png" alt="image-20200730150129752" style="zoom:80%;" />

先不用太感動，我們還沒讓世界看到呢 !!



---



# Upload to GitLab

上傳至GibLab

先改一下 web資料夾下的 **_config.yml**

```python
root: /  ->  root: /mynotes/  # mynotes 是你的專案(Repository)名稱
```



在 web資料夾下的  **小黑窗**，上方的 **小黑窗** 不要關掉的狀態

先確認自己的使用者名稱和信箱是否有設定完成

```python
git init  # 交由git代管

git config --global user.name "your_user_name"  
# your_user_name 是 gitlab 中的username
git config --global user.email "your_email"
# your_email 是 gitlab 中的email
```

如果沒輸入，會跑出這樣的訊息，要求你輸入

<img src="../images/BuildWebsite/image-20200730152546339.png" alt="image-20200730152546339" style="zoom:80%;" />

進行遠端連線的設定

```python
git remote add origin "https://gitlab.com/mrdanielnotes/mynotes.git"
# origin 可自行定義，預設為origin
git remote  #可看到擁有那些連線
```

進行更新、新增和上傳

```python
git pull origin master  # 本機(你的電腦) 將你的web更新至最新版本，可能有其他檔案有變更

git add .  # 一次上傳所有變更

git commit -m "my first website"  
# 上傳至本機 (也就是暫存在你的電腦裡面，確定要上傳的項目)
# my first website 為此次上傳，自行定義的註解

git push origin master  # 上傳

# 接著會要求你輸入帳號密碼，你自己的GitLab Username與密碼
```

(看到有再跑進度條，然後停止，就代表上傳成功，笑)

大概長這樣

<img src="../images/BuildWebsite/image-20200730180149417.png" alt="image-20200730180149417" style="zoom:80%;" />

在你的GitLab上，會多出很多項目

<img src="../images/BuildWebsite/image-20200730173515739.png" alt="image-20200730173515739" style="zoom:80%;" />

最後，新增佈署檔案 ***.gitlab-ci.yml***  ，快完成了，再撐一下

在 GitLab的 Repository 中，新增一個檔案

<img src="../images/BuildWebsite/image-20200730173844939.png" alt="image-20200730173844939" style="zoom:80%;" />



<img src="../images/BuildWebsite/image-20200730174236682.png" alt="image-20200730174236682" style="zoom:80%;" />

代碼如下

```python
image: node:10-alpine # use nodejs v10 LTS
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```

左方目錄欄，選擇，**CI / CD** > **Pipelines**  

會看到正在執行

<img src="../images/BuildWebsite/image-20200730174601243.png" alt="image-20200730174601243" style="zoom:80%;" />

過一會兒後，看到passed，代表是通過

<img src="../images/BuildWebsite/image-20200730174707604.png" alt="image-20200730174707604" style="zoom:80%;" />

再到左方目錄欄，選擇 **Settings** > **Pages**

<img src="../images/BuildWebsite/image-20200730174954596.png" alt="image-20200730174954596" style="zoom:80%;" />

二話不說，直接點開

<img src="../images/BuildWebsite/image-20200730175547425.png" alt="image-20200730175547425" style="zoom:80%;" />

然後就可以發給你朋友了。



# Conclusion

結論

架設站台，就是這樣點一點就完成了，不需要網域、不需要付費

之後再撰寫如何更改樣式、中文、畫面

---


