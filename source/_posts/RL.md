---
title: Reinforcement Learning 強化式學習
date: 2020-08-05 11:25:52
tags: [Machine Learning, ML x RL]
categories: [Artificial intelligence]
---

不同的環境，學習不同的決策方式，近代的常見範例為

自動駕駛 (Auto Driver)、Alpha go、機器人走路、遊戲上的學習等

<!-- more -->



## 什麼是 *強化式學習*

強化式學習 (Reinforcement Learning, RL)，淺白的意義為，從無到有的學習。

分為幾個項目介紹

1. 動作者(Actor) - 執行動作的物體
2. 動作(Action) - 執行動作
3. 獎勵/懲罰(Reward) - 獎懲機制
4. 環境/環境更新(Environment) - 環境的變化
5. 記憶(Memory) - 儲存

舉例來說，像小時候學習騎腳踏車，不停的摔倒，其實在肌肉、記憶正在學習如何不摔倒

<img src="../images/RL/image-20200805114713720.png" alt="image-20200805114713720" style="zoom: 25%;" />

根據上述項目帶入舉例中

1. 動作者(Actor) = 小時候的我

2. 動作(Action) = 左右晃動，為了使自己不跌倒

3. 獎勵/懲罰(Reward) = 保持在腳踏車上(獎勵) / 跌倒受傷(懲罰)

4. 環境/環境更新(Environment) = 有點偏右、有點偏左等狀態表示

5. 記憶(Memory) = 腦部、肌肉、方式、踩踏角度等

   

## 程式範例

講完概念，來一個簡單的程式範例，搭配剛剛的概念會更清楚

<img src="../images/RL/books_read00.png" alt="books_read00" style="zoom: 67%;" />

一個找 男/女朋友的概念

1. 動作者(Actor) = 紅點 (搜尋方)
2. 動作(Action) = 上下左右
3. 獎勵/懲罰(Reward) = 找到人生中另一半 (獎勵) / 無懲罰
4. 環境/環境更新(Environment) = 根據動作(Action)，更新當前紅點所在的位置
5. 記憶(Memory) = 第一個路口要左轉、第二個路口要右轉等等



## 訓練&成果

epoch(訓練次數-迴圈)，step_counter(走了幾步)，訓練第4次的時候，走的步數突然大幅降低，這代表搜尋方已找到訣竅，能夠快速通關找到另一半

<img src="../images/RL/image-20200805171151482.png" alt="image-20200805171151482" style="zoom: 67%;" />

訓練過程視覺化顯示

跑到第十次訓練的時候，看到紅點走的規則已經趨近於穩定，不太會有太多的變化了

這樣的一個路徑就是他認定的最佳路徑

概念有點像是人類每天回家或上班的路，可能都會依據慣性、習慣走的一條路，不會刻意去改變一樣

除非遇到障礙物..... (之後我們在寫一篇，怎麼實作障礙物的部分)

<img src="../images/RL/animated-1596616871423.png" alt="animated" style="zoom:67%;" />

想要了解更多細節，可以到我的 **[GitLab](https://gitlab.com/mrdanielnotes/python_demo)** Fork 或 Clone自己玩玩看



如果

經過上述解釋，還是一知半解，沒關係，至少把概念帶走

強化式學習 (Reinforcement Learning) 在玩遊戲上的應用也是很常見的，正所謂用 **程式玩程式**

<iframe width="560" height="315" src="https://www.youtube.com/embed/qv6UVOQ0F44" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



## 結語

強化式學習能夠看出訓練變化，也能實體展現成果，在應用上較有亮點，實作起來也很有成就感

希望原本不懂，可以帶走一些概念和思考模式



本人也在學習中，大家互相討論，互相成長

這是我認知上的 **強化式學習** ，如果任何錯誤或觀念上問題，還請多多包含


