---
title: Typora基本操作教學
date: 2020-08-03 11:22:30
tags: [typora]
categories: [免費架站]
---

以最簡單、快速的方式操作 Typora

<!-- more -->



# 前置作業

準備工具：[Typora  windws版](https://typora.io/#windows) [Typora linux版](https://typora.io/#linux) 或者是前往 [Typora](https://typora.io) 找尋相關的配件

---



# 基本操作

## 斜體

```python
ctrl + i
```

*這是斜體*

為什麼是 + i，因為斜體的英文是 Italic，Microsoft Office系列也都可以這樣用

---



## 粗體

```
ctrl + b
```

**這是粗體**

為什麼是 + b，因為斜體的英文是 bold，Microsoft Office系列也可以用 (偷笑)

---



## 標題

````python
ctrl + 1, 2, 3, 4, 5, 6
````

標題1, 2, 3, 4, 5, 6的概念，可針對單行操作

也可以使用階層的方式操作

增階

```python
ctrl + =
```

減階

```python
ctrl + -
```

---



## 程式碼區塊

```
ctrl + shift + k
```

方框處，直接打下想打的程式區塊或語言，並在右下角選擇語言

![image-20200722145540985](../images/typora-images/image-20200722145540985.png)

像這樣，美美的python就這樣完成了，是不是很方便阿

![image-20200722150338210](../images/typora-images/image-20200722150338210.png)

結果呈現

```python
for i in range(5):
	print(i)
```

也可以使用 ````python 也會有一樣的效果



## 公式區塊

```python
ctrl + shift + m
```

![image-20200722151137441](../images/typora-images/image-20200722151137441.png)



結果呈現

$$
   volumn = height * width
$$


   ```python
   $$ + enter
   ```

   也會有一樣的效果唷

   

## 插入圖片

```python
ctrl + shift + i
```



"這是範例圖片"，的文字內容則是，滑鼠只在圖片上會出現的提示訊息

接著，應該不用多說了，就在"輸入圖片網址"，輸入圖片網址，OK，沒問題

![image-20200722151340965](../images/typora-images/image-20200722151340965.png)





結果呈現

<img src="https://cdn.jsdelivr.net/gh/Rhinocros/digit77-static-cdn@pics2/uPic/HbmHIM.png" alt="這是範例圖片" style="zoom: 33%;" />

```python
ctrl + c, ctrl + v
```

雖然也可以適用，不過要記得把圖片另外存在雲端空間，匯出的時候，本身電腦看得到，但傳給其他人圖片就會通通不見囉。

![image-20200722152008376](../images/typora-images/image-20200722152008376.png)



## 插入超連結

```python
ctrl + k
```



這裡很容易搞混，前面介紹的是程式碼區塊

```python
ctrl + shift + k
```

先打一段文字

這是超連結

直接按下

```python
ctrl + k
```

[這是超連結]()



![image-20200722152800228](../images/typora-images/image-20200722152800228.png)

再把連結複製到後方的 ()中，即可完成

![image-20200722152716921](../images/typora-images/image-20200722152716921.png)





結果呈現

[這是超連結](../)



## 產生綱要 (目錄)

```python
[TOC] + enter
```

直接打[TOC]，就會變出來了，根據標題產生目錄，hen棒棒！！





# 結語

打完收工，以上就是這次的基礎教學，也方便我記錄一下學習，希望能夠對你有幫助

